FROM ubuntu:14.04
LABEL Thierry Boulord, thierry.boulord@gmail.com
#install Java JRE 8
ENV JAVA_DIR /usr/java
ARG SERVER_PORT
ARG SERVER_RMI_LOCALPORT
ARG OFFSET

ENV ENV_SERVER_PORT=${SERVER_PORT}
ENV ENV_SERVER_RMI_LOCALPORT=${SERVER_RMI_LOCALPORT}
ENV ENV_OFFSET=${OFFSET}
ENV http_proxy=${http_proxy}
ENV https_proxy=${https_proxy}

RUN apt-get update && \
  apt-get install -y software-properties-common && \
  add-apt-repository -y ppa:openjdk-r/ppa && \
  apt-get update && \
  apt-get install -y openjdk-8-jre-headless wget && \
  mkdir ${JAVA_DIR} && \
  ln -s /usr/lib/jvm/java-8-openjdk-amd64 ${JAVA_DIR}/default




#install JMeter 5.1.1
ENV JMETER_VERSION 4.0
ENV JMETER_DIR apache-jmeter-${JMETER_VERSION}
ENV JMETER_ARCHIVE ${JMETER_DIR}.tgz
ENV JMETER_URL https://archive.apache.org/dist/jmeter/binaries/${JMETER_ARCHIVE}
RUN wget -q ${JMETER_URL} && \
  tar -xzf ${JMETER_ARCHIVE} -C /var/lib && \
  mv /var/lib/${JMETER_DIR} /var/lib/jmeter && \
  rm -rf ${JMETER_ARCHIVE}

#install JMeter-plugin
RUN wget -P /var/lib/jmeter/lib/ext --trust-server-names https://jmeter-plugins.org/get/
RUN wget -O /var/lib/jmeter/lib/ext/jmeter-plugins-manager-0.19.jar  "http://search.maven.org/remotecontent?filepath=kg/apc/jmeter-plugins-manager/0.19/jmeter-plugins-manager-0.19.jar"
RUN wget -O /var/lib/jmeter/lib/cmdrunner-2.0.jar  "http://search.maven.org/remotecontent?filepath=kg/apc/cmdrunner/2.0/cmdrunner-2.0.jar"
RUN java -cp /var/lib/jmeter/lib/ext/jmeter-plugins-manager-*.jar org.jmeterplugins.repository.PluginManagerCMDInstaller
RUN /var/lib/jmeter/bin/PluginsManagerCMD.sh install jpgc-standard,bzm-random-csv,jpgc-sts

# ADD all the plugins
#ADD /lib/ext /var/lib/jmeter/lib/ext

# ADD csv
# ADD /csv /var/lib/jmeter/bin/_FIDUCIAL

# ADD keystore
ADD /keystore /var/lib/jmeter/bin/

#install sshd
#RUN apt-get update
RUN apt-get install -y openssh-server && \
    mkdir /var/run/sshd && \
    echo 'root:root' |chpasswd && \
    sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config

#User properties
RUN echo " .\n\
	server_port=$ENV_SERVER_PORT\n\
	server.rmi.localhostname=127.0.0.1\n\
	server.rmi.localport=$ENV_SERVER_RMI_LOCALPORT\n\
	server.rmi.ssl.disable=true\n\
  offset=$ENV_OFFSET\n\
  server.rmi.ssl.keystore.type=JKS\n\
  server.rmi.ssl.keystore.file=rmi_keystore.jks\n\
  server.rmi.ssl.keystore.password=password\n\
  server.rmi.ssl.keystore.alias=rmi\n\
  server.rmi.ssl.truststore.type=JKS\n\
  server.rmi.ssl.truststore.file=rmi_keystore.jks\n\
  server.rmi.ssl.truststore.password=password\n\
  server.rmi.ssl.disable=true" >> /var/lib/jmeter/bin/user.properties

#install supervisor
RUN apt-get install -y supervisor && \
    mkdir -p /var/lock/apache2 /var/run/apache2 /var/run/sshd /var/log/supervisor
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN ln -sf /dev/stdout /jmeter-server.log
#COPY user.properties /var/lib/jmeter/bin/user.properties

#cleanup
RUN rm -rf /var/lib/apt/lists/* ${JMETER_ARCHIVE}

EXPOSE 22 $ENV_SERVER_PORT 25000

CMD ["/usr/bin/supervisord"]
