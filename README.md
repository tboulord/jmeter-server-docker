
JMeter Server
===============

Forked from flaviostutz/jmeter-server


Use this container in order to execute load tests using JMeter remotelly with custom port forwarding from bash.

To use this container:
   - Install JMeter 4.0 on your machine and run it using Java 8
   - Run this container in a remote host
      - git clone https://github.com/ThierryBoulord/jmeter-server.git
      - Build and run with custom port : 
	        'SERVER_PORT=<server_port> SERVER_RMI_LOCALPORT=<server_rmi_localport> docker-compose up'
	  - create a SSH tunnel from Putty :
			-L <server_port>:127.0.0.1:<server_port> 
			-R 25000:127.0.0.1:25000 
			-L <server_rmi_localport>:127.0.0.1:<server_rmi_localport>
      - Start JMeter on your machine, create a Test Plan and execute the plan through Execute -> Execute Remote -> "localhost:<server_port>"

Reference: https://cloud.google.com/compute/docs/tutorials/how-to-configure-ssh-port-forwarding-set-up-load-testing-on-compute-engine/
